import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ItemService } from "../../Services/item.service";

//GSAP declaration and requires
declare var require: any;
const TimelineLite = require('gsap').TimelineLite;
const TweenLite = require('gsap').TweenLite;
const Power1 = require('gsap').Power1;

@Component({
  selector: 'checkout-form',
  templateUrl: './checkout-form.component.html',
  styleUrls: ['./checkout-form.component.scss']
})
export class CheckoutFormComponent implements OnInit {

  selectedCartType: HTMLElement

  constructor(
    private router: Router,
    private itemService: ItemService
  ) { }

  ngOnInit() {
  }

  /**
   * Preceec to GSAP Timelinelite() Animation
   * @param form The form to check validity
   */
  initTimeline(form) {
    if (form && form.checkValidity()) {
      var tl = new TimelineLite();
      tl.to('.btn-label', .1, { autoAlpha: 0, display: 'none' }, 'start');
      tl.to('.btn-label', 0, { display: 'none' });
      tl.to('.spinner', 0, { display: 'block' });
      tl.to('.btn-label', .1, { autoAlpha: 1 });
      tl.to('.frm-submit', .2, { ease: Power1.easeOut, width: 50 }, 'start');
      TweenLite.set('shopping-cart', { overflow: 'hidden' });
      tl.to('.frm-submit', .2, { ease: Power1.easeIn, width: 3000, height: 3000, borderRadius: '50%', delay: 2 });
      tl.to('.spinner', 0, { display: 'none' });
      tl.to('.frm-submit', .2, { ease: Power1.easeIn, autoAlpha: 0, delay: .1 }, 'maskLogin');

      // tl.call(() => { this.router.navigate(['/succes']); }); //seems to not work...
      setTimeout(() => {
        this.router.navigate(['/succes']);
        this.itemService.resetToInitialItems()
      }, 2400);
    }
  }
  /**
   * Check if an Input is empty and add him a class
   * @param elm The input to check
   */
  onInputChange(elm: HTMLInputElement) {
    if (elm.value != '') elm.classList.add('not-empty');
    else elm.classList.remove('not-empty');
  }

  /**
   * Set the selected Element
   * @param elm The selected element 
   */
  setElmSelectedCartType(elm: HTMLButtonElement) {
    event.preventDefault()
    this.selectedCartType = elm
  }
  isSelected(id: string): boolean {
    return this.selectedCartType && this.selectedCartType.id == id
  }

}
