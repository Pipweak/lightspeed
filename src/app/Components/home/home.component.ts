import { Component, OnInit } from '@angular/core';
import { ItemService } from "../../Services/item.service";
import { Location } from '@angular/common';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    private itemService: ItemService,
    private location: Location
  ) {}

  ngOnInit() {
    this.itemService.initItems(); 
  }
  /**
   * Manage the display of shopping card component 
   * regaring to location path
   */
  showCart(): boolean {
    return this.location.path() !== '/cart' && this.location.path() !== '/404' && this.location.path() !== '/succes'  ? true : false
  }

}
