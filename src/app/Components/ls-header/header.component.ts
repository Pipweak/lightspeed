import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'ls-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  path: string;

  constructor(
    private location: Location
  ) {  }

  ngOnInit() {
  }

  /**
   * Check if location path is Home.
   *    .Used to display Burgermenu or left arrow 
   */
  isHome(): boolean {
    return this.location.path() === '/home' ? true : false
  }

  /**
   * Navigate to last location in browser history
   */
  goBack(): void {
    this.location.back();
  }
}
