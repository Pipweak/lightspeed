import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ItemService } from "../../Services/item.service";
import { Item } from '../../Models/Item';
import { Subscription } from 'rxjs';

@Component({
  selector: 'store-item-detail',
  templateUrl: './store-item-detail.component.html',
  styleUrls: ['./store-item-detail.component.scss']
})
export class StoreItemDetailComponent implements OnInit, OnDestroy {

  item: Item
  subscription: Subscription;

  constructor(
    private route: ActivatedRoute,
    private itemService: ItemService,
  ) { }

  ngOnInit(): void {
    this.subscription = this.itemService.itemsSubj.subscribe(items => {    //On init, On listen l'arrivee des datas
      if (items) {
        this.getItem();                                                     //A la reception des datas on get l'item/{id}  
      }
    })
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  /**
   * Get the datas of the item with specific _id
   * return void
   */
  getItem(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.item = this.itemService.getItemById(id);
  }

  /**
   * Fonction passe-plat vers le service itemService.
   * @param item l'item a ajouter.
   */
  addItemToCart(item: Item) {
    this.itemService.addItemToCart(item);
    this.itemService.triggerAnimationToCart();
  }
}
