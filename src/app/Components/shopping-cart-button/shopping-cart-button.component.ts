import { Component, OnInit, Input } from '@angular/core';
import { CartItem } from '../../Models/Item';
import { ItemService } from "../../Services/item.service";
import * as _ from 'lodash';

@Component({
  selector: 'shopping-cart-button',
  templateUrl: './shopping-cart-button.component.html',
  styleUrls: ['./shopping-cart-button.component.scss']
})
export class ShoppingCartButtonComponent implements OnInit {

  constructor(
    private itemService: ItemService
  ) { }

  ngOnInit() {
    if (!this.checkIsCartEmpty(this.itemService.items as CartItem[])) this.itemService.setCartNotification();
  }

  /**
   * Check if a card is empty regarding to item quantities
   * @param items the array of items
   */
  checkIsCartEmpty(items: CartItem[]): boolean {
    return _.find(items, (item: CartItem) => { return item.quantity && item.quantity > 0; }) == undefined ? true : false;
  }
}
