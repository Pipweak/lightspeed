import { Component, OnInit, OnDestroy } from '@angular/core';
import { ItemService } from "../../Services/item.service";
import { Item, CartItem } from '../../Models/Item';
import { Subscription } from 'rxjs';

@Component({
  selector: 'shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss']
})
export class ShoppingCartComponent implements OnInit, OnDestroy {

  items: Item[]
  subscription: Subscription;

  constructor(private itemService: ItemService) { }

  ngOnInit() {
    this.subscription = this.itemService.itemsSubj.subscribe(items => {
      this.items = items;
    })
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  /**
   * Ask to itemService to  Increment the count of particular Item in cart
   * @param item the count of particular Item to increment
   */
  incrementItem(item: Item) {
    this.itemService.incrementItem(item)
  }

  /**
   * Ask to itemService to Decrement the count of particular Item in cart
   * @param item the Item to decrement
   */
  decrementItem(item: Item) {
    this.itemService.decrementItem(item)
  }

  /**
   * Ask to itemService to remove Item from cart
   * @param item the Item to remove
   */
  removeItem(item) {
    this.itemService.removeItem(item)
  }

  /**
   * Manage the quantity of an Item
   * @param elm the input
   * @param item the item to manage
   */
  onQuantityChange(elm: HTMLInputElement, item: Item) {
    if (elm.value != '') {
      let searchValue = Number(elm.value)
      if (searchValue === 0) {
        this.removeItem(item)
      } else if (!isNaN(searchValue)) {
        let cartitem = this.itemService.getItemById(item._id) as CartItem
        let startStock = cartitem.stock.remaining + cartitem.quantity
        if (startStock - searchValue >= 0) {  // assez de stock
          cartitem.stock.remaining = startStock - searchValue
          cartitem.quantity = searchValue
        } else {
          elm.value = cartitem.quantity.toString()
        }
      }
    }
  }
}
