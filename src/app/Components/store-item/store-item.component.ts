import { Component, OnInit, Input } from '@angular/core';
import { Item } from '../../Models/Item';
import { ItemService } from "../../Services/item.service";

@Component({
  selector: 'store-item',
  templateUrl: './store-item.component.html',
  styleUrls: ['./store-item.component.scss']
})
export class StoreItemComponent implements OnInit {

  @Input() item: Item;


  constructor(
    private itemService: ItemService
  ) { }

  ngOnInit() { }

  /**
   * Fonction passe-plat vers le service itemService.
   * @param item l'item a ajouter.
   */
  addItemToCart(item: Item, target:any) {
    this.itemService.addItemToCart(item);
    this.itemService.triggerAnimationToCart();
  }

}
