import { Component, OnInit, OnDestroy } from '@angular/core';
import { Item } from '../../Models/Item';
import { ItemService } from "../../Services/item.service";
import { Subscription } from 'rxjs';

@Component({
  selector: 'ls-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit, OnDestroy {

  items: Item[]
  subscription: Subscription;

  constructor(private itemService: ItemService) { }

  ngOnInit() {
    this.subscription = this.itemService.itemsSubj.subscribe(items => {
      this.items = items;
    })
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }


}
