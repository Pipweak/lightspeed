import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Item } from '../Models/Item';

@Injectable({
  providedIn: 'root'
})
export class RequestService {
  private baseUrl = 'http://localhost:3000/lightspeed';

  constructor(private http: Http) { }

  getItems():  Promise<Item[]> {
    return this.http.get(this.baseUrl)
      .toPromise()
      .then(response => response.json() as Item[])
      .catch(this.handleError);
  }

  getItem(id):  Promise<Item> {
    return this.http.get(this.baseUrl + "?_id=" + id)
      .toPromise()
      .then(response => response.json() as Item)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('Some error occured', error);
    return Promise.reject(error.message || error);
  }
}
