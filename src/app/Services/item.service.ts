import { Injectable } from '@angular/core';
import { Item, CartItem } from '../Models/Item';
import { RequestService } from "./request.service";
import { BehaviorSubject } from 'rxjs';
import * as _ from 'lodash';


@Injectable({
  providedIn: 'root'
})
export class ItemService {

  items: Item[]
  itemsSubj = new BehaviorSubject<Item[]>([]);  // Init BehaviorSubject to listen to Items and getting last value if exist.

  constructor(
    private rqService: RequestService
  ) { }

  /**
   * Demande au service rqService l'init du tableau d'Item.
   */
  initItems() {
    this.rqService.getItems().then(items => {
      this.items = items;
      this.emitItems(items)
    });
  }
  
  /**
   * Reset the Items to initial state after Checkout
   */
  resetToInitialItems(){
    this.initItems();
  }

  /**
   * Emit la nouvelle version du tableau d'Item
   * @param items La nouvelle version du tableau d'Item.
   */
  emitItems(items) {
    this.itemsSubj.next(items)
  }

  /**
   * Return l'item requested by his id.
   * @param id l'id de l'item a return.
   */
  getItemById(id): Item { // Use lodash pour ici
    return _.find(this.items, ['_id', id]);
  }

  /**
   * Check if item is already into the cart.
   *    If yes increment the quantity,
   *    else push the property quantity into Item
   *    then decrement the stock and emit the new Items Array
   * @param item l'item a ajouter.
   */
  addItemToCart(item: Item) {
    let addedItem = this.getItemById(item._id) as CartItem;
    !addedItem.hasOwnProperty('quantity') ? addedItem['quantity'] = 1 : addedItem.quantity++;
    addedItem.stock.remaining--;
    this.emitItems(this.items);
  }

  /**
   * Trigger the checkout animation
   */
  triggerAnimationToCart() {
    const elm = document.querySelector('.shopping-cart-wrapper') as HTMLElement;
    this.setCartNotification()
    elm.classList.remove("active");
    void elm.offsetWidth;
    elm.classList.add("active");
  }

  /**
   * Set notification of non empty cart on shopping cart button
   */
  setCartNotification() {
    const elm = document.querySelector('.shopping-cart-wrapper') as HTMLElement;
    if (elm.className !== 'marked') elm.classList.add('marked');
  }

   /**
   * Increment the count of particular Item in cart
   * @param item the count of particular Item to increment
   */
  incrementItem(item: Item) {
    let itemToUpdate = this.getItemById(item._id) as CartItem;
    if (itemToUpdate.hasOwnProperty('quantity')) {
      itemToUpdate.quantity++;
      itemToUpdate.stock.remaining--;
      this.emitItems(this.items);
    }
  }

  /**
   * Decrement the count of particular Item in cart
   * @param item the Item to decrement
   */
  decrementItem(item: Item) {
    let itemToUpdate = this.getItemById(item._id) as CartItem;
    if (itemToUpdate.hasOwnProperty('quantity')) {
      itemToUpdate.quantity--;
      itemToUpdate.stock.remaining++;
      this.emitItems(this.items);
    }
  }

  /**
   * Remove Items from itemServices' List
   * @param item the Item to remove
   */
  removeItem(item) {
    let itemToUpdate = this.getItemById(item._id) as CartItem;
    if (itemToUpdate.hasOwnProperty('quantity')) {
      itemToUpdate.stock.remaining += itemToUpdate.quantity;
      itemToUpdate.quantity = 0;
      this.emitItems(this.items);
    }
  }


}
