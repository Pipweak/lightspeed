import { Pipe, PipeTransform } from '@angular/core';
import { CartItem } from '../Models/Item';
import * as _ from 'lodash';

@Pipe({ name: 'cartTotal', pure: false })
export class CartTotalPipe implements PipeTransform {

  transform(items: CartItem[]): number {
    let arrayOfPrices = _.map(items, this.price);
    let sum = _.sum(arrayOfPrices)
    return Number(sum.toFixed(2));
  }

  price(item: CartItem) {
    let qty = Number(item.quantity);
    let price = Number(item.price.substring(1, item.price.length).replace(',', ''));
    return price * qty;
  }
}
