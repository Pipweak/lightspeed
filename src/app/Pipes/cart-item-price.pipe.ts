import { Pipe, PipeTransform } from '@angular/core';
// import { Item } from '../Models/Item';

@Pipe({ name: 'cartItemPrice' })
export class CartItemPricePipe implements PipeTransform {

  transform(value: string, qty: number): number {
    let price = Number(value.substring(1, value.length).replace(',', ''));
    
    return Number((price * qty).toFixed(2));
  }

}
