import { Pipe, PipeTransform } from '@angular/core';
import { CartItem } from '../Models/Item';

@Pipe({ name: 'inCartItems' , pure: false})
export class inCartItems implements PipeTransform {
  
  transform(item: CartItem[]) {
    return item.filter(item => (item.hasOwnProperty('quantity') && item.quantity > 0) ); 
  }
}
