import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './Components/home/home.component';
import { HeaderComponent } from './Components/ls-header/header.component';
import { ContentComponent } from './Components/ls-content/content.component';
import { ShoppingCartButtonComponent } from './Components/shopping-cart-button/shopping-cart-button.component';
import { StoreItemComponent } from './Components/store-item/store-item.component';
import { AppRoutingModule } from '../app-routing.module';
import { StoreItemDetailComponent } from './Components/store-item-detail/store-item-detail.component';
import { ShoppingCartComponent } from './Components/shopping-cart/shopping-cart.component';

import { inCartItems } from './Pipes/in-cart.pipe';
import { CartItemPricePipe } from './Pipes/cart-item-price.pipe';
import { NotFoundComponent } from './Components/not-found/not-found.component';
import { CartTotalPipe } from './Pipes/cart-total.pipe';
import { SuccesComponent } from './Components/succes/succes.component';
import { CheckoutFormComponent } from './Components/checkout-form/checkout-form.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    ContentComponent,
    ShoppingCartButtonComponent,
    StoreItemComponent,
    StoreItemDetailComponent,
    ShoppingCartComponent,
    inCartItems,
    CartItemPricePipe,
    NotFoundComponent,
    CartTotalPipe,
    SuccesComponent,
    CheckoutFormComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AppRoutingModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
