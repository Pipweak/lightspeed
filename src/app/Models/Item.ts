export class Item {
    _id: string;
    title: string;
    description: string;
    stock: {
      remaining:number;
    };
    price: string;
    image: string;
    color: string;
  }

  export class CartItem extends Item {
    quantity: number;
  }