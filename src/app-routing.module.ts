import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { StoreItemDetailComponent } from "./app/Components/store-item-detail/store-item-detail.component";
import { ContentComponent } from "./app/Components/ls-content/content.component";
import { ShoppingCartComponent } from './app/Components/shopping-cart/shopping-cart.component';
import { NotFoundComponent } from './app/Components/not-found/not-found.component';
import { SuccesComponent } from './app/Components/succes/succes.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: ContentComponent },
  { path: 'detail/:id', component: StoreItemDetailComponent },
  { path: 'cart', component: ShoppingCartComponent }, 
  { path: 'succes', component: SuccesComponent }, 
  {path: '404', component: NotFoundComponent},
  {path: '**', redirectTo: '/404'}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}