# Lightspeed
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.0.

## Step 1. Prerequisites.

You need to set up your development environment before you can do anything.

Install [Node.js and npm](https://nodejs.org/en/download/) if they are not already on your machine.

> Verify that you are running at least Node.js version 8.x or greater and npm version 5.x or greater by running node -v and npm -v in a terminal/console window. Older versions produce errors, but newer versions are fine.

## Step 2. Create a new project based on this Repo.

Clone this repo into new project folder (e.g., my-proj).

```
git clone https://Pipweak@bitbucket.org/Pipweak/lightspeed.git  my-proj
cd my-proj
```

## Step 3. Install JSON Server
To simulate a full fake REST API we are using [json-server](https://github.com/typicode/json-server).

### Installing JSON Server
JSON Server is available as a NPM package. The installation can be done by using the Node.js package manager:

`npm install -g json-server`

###	Running The Server
Then start JSON server by executing the following command:

`json-server --watch db.json`

## Step 4. Install npm packages

Install the npm packages described in the package.json and verify that it works:

```
npm install
ng serve --open
```

The `ng serve` command launches the server, watches your files, and rebuilds the app as you make changes to those files.

Using the --open (or just -o) option will automatically open your browser on `http://localhost:4200/`.

# Annexes
### Technos
Angular 6

HTML5

CSS3

### Tools 
VisualStudio Code

Cmder

Source Tree

JSON Server for data.

Git, Bitbucket

### Sources
[JSON Server](https://medium.com/codingthesmartway-com-blog/create-a-rest-api-with-json-server-36da8680136d)
Also Many inspiration on [Dribbble](https://dribbble.com/), [Behance](https://www.behance.net/)]...

### Responsiveness
Simulated with Chrome 

Phone: on IphoneX

Tablet: iPad / iPad Pro